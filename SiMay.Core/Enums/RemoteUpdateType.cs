﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core.Enums
{
    public enum RemoteUpdateType
    {
        Url,
        File
    }
}
